<?php

/**
 * Connect to Database
 */ 
function init($query){    
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "BKMusic";

    // Create connection
    $conn = new mysqli($servername, $username, $password,$dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 
    $conn->set_charset("utf8"); 
    
    getResult($conn, $query);
    $conn->close();
 }
 function getResult($conn, $query){
       
        /**
         * for example "http://localhost/bkmusic/views/layouts/search.html?search=a&page=1&sort=name%20desc"
         * TODO explain
         */
         

        $urlComponent = explode("&", $query); // 1

        $sort = explode(":", $urlComponent[2])[1]; // 2
        $sortKey =explode("%20", $sort)[0]; // 3
        $sortValue = explode("%20", $sort)[1]; // 4   
        $search =explode("=",$urlComponent[0])[1]; // 5
        $query = urldecode(explode(":",$search)[1]); // 6
        $pageNo = explode(":", $urlComponent[1])[1]; // 7
        $page_result = 5;
        $offset = 0;
        
        if($pageNo > 1)
        {	
            $offset = ($pageNo - 1) * $page_result;
        }
        $rows = array();
  
        //TODO remmove %
        $sql = "SELECT *, (select count(*) from songs where title LIKE '$query%') as total FROM songs WHERE title LIKE '%$query%' ORDER BY $sortKey $sortValue limit $offset, $page_result";
        $result = $conn->query($sql);  
        if ($result->num_rows > 0) {            

            while ($row = mysqli_fetch_assoc($result)) {
                    
                $rows[] = $row;


            }
           
            $obj = (object) [
                'content' => $rows,
                'total' =>  $rows[0]['total'],
                'limit' =>  $page_result
            ];
            
            echo json_encode($obj);  

        }
        else {  
            $sql = "SELECT COUNT(id) as total FROM songs WHERE title LIKE '$query%'";
            $result = $conn->query($sql);  
            if ($result->num_rows > 0) {            

                while ($row = mysqli_fetch_assoc($result)) {
                        
                    $rows[] = $row;
    
    
                }
               
                $obj = (object) [
                    'content' => null,
                    'total' =>  $rows[0]['total'],
                    'limit' =>  $page_result
                ];
                
                echo json_encode($obj);  
    
            }
            else{
                $obj = (object) [
                    'content' => null,
                    'total' =>  0,
                    'limit' =>  $page_result
                ];
                
                echo json_encode($obj);  
            }
  

        }


    
 }
$link = parse_url($_SERVER['REQUEST_URI']);    
init($link['query']);


?>