<?php
session_start();

function deleteSession($mail, $conn){
    $sql =  "UPDATE users SET sessionString = null, sessionId = null WHERE mail='$mail'";
    $conn->query($sql);
}

function checkSession($mail,$sessionId, $sessionString, $conn) {
    if($sessionString===""){//destroy session
        session_unset();
        session_destroy();
        unset($_COOKIE['PHPSESSID']);
        setcookie('PHPSESSID', '', time() - 3600, '/');
        deleteSession($mail, $conn);
        echo null;
        //DELETE SESSION STRING
    
    } else {
        $_SESSION["mail"] = $mail;
        $expireTime = 120;
    
        $sql = "SELECT * FROM users WHERE sessionString='$sessionString' AND sessionId='$sessionId' LIMIT 1;";    
        $result = $conn->query($sql);     
        
        $row = null;
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();         
        } 
        
        echo json_encode($row);      
   
        setcookie("mail",$mail, time() + (86400 * 30*365), "/");
        setcookie("name",$row['fullname'], time() + (86400 * 30*365), "/");
        setcookie("sessionString",$sessionString,time()+ $expireTime,"/");
    }
}

function init($mail,$sessionId, $sessionString){    
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "BKMusic";
    // Create connection
    $conn = new mysqli($servername, $username, $password,$dbname);
    $conn->set_charset("utf8"); 
    
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 
    
    checkSession($mail,$sessionId, $sessionString, $conn);

    
    $conn->close();
}

init($_POST["mail"],$_POST["sessionId"],$_POST["sessionString"]);