/**
 * This page require login to query data!
 * Check user login or not
 */

window.onload = function () {
    /** If not login */
    if (getCookie("PHPSESSID") == '') {

        document.getElementById("page-ctn").innerHTML = "<div id='authenticate'><a>You must login to view this page!</a></div><div  id='login-btn'><button type='button' class='btn btn-primary' data-toggle='modal' data-target='#loginModal'>Log in!</button></div>"


    }
    else {
        //if login but session timeout
        if (getCookie("sessionString") == '') {
            document.getElementById("page-ctn").innerHTML = "<div id='authenticate'><a>Session time out! Please log in again</a></div><div  id='login-btn'><button type='button' class='btn btn-primary' data-toggle='modal' data-target='#loginModal'>Log in!</button></div>"
        }
        //If session timeout then send data to server to destroy session
        //Else query data
        $.ajax({
            type: 'POST',
            url: "../../servers/getProfile.php",
            data: {
                'mail': getCookie("mail"),
                'sessionId': getCookie("PHPSESSID"),
                'sessionString': getCookie("sessionString"),
            },

            success: function (queryData, textStatus, jqXHR) {

                if (queryData == "destroy") {
                    window.location = '../layouts/home-page.html';
                }
                else {
                    var info = JSON.parse(queryData)
                    console.log(info)
                    document.getElementById("username").placeholder = info['fullname']
                    document.getElementById("email").placeholder = info['mail']
                    document.getElementById("currentpass").value = "nacute"
                    document.getElementById("newpass").value = "nacute"
                    document.getElementById("memtype").value = info['type']
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Path Finder : ' + textStatus);
            }
        });
    }
}


/**
 * Support function
 * Reference: w3school 
 */
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}