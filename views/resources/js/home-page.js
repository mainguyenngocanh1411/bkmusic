window.onload = function () {
    //load layout
    includeHTML();

    //load chart
    getChart();

    //sidebar effect
    $(".recommend-playlist").hover(function () {
        $(".recommend-playlist").css("opacity", 1);

    }, function () {
        $(".recommend-playlist").css("opacity", 0.7);

    });
    $(".pop").hover(function () {
        $(".pop").css("opacity", 1);

    }, function () {
        $(".pop").css("opacity", 0.7);

    });
    $(".rock").hover(function () {
        $(".rock").css("opacity", 1);

    }, function () {
        $(".rock").css("opacity", 0.7);

    });
    $(".acoustic").hover(function () {
        $(".acoustic").css("opacity", 1);

    }, function () {
        $(".acoustic").css("opacity", 0.7);

    });
    $(".ballad").hover(function () {
        $(".ballad").css("opacity", 1);

    }, function () {
        $(".ballad").css("opacity", 0.7);

    });
}

function getChart() {
    //Get chart
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {

        if (this.readyState == 4 && this.status == 200) {
            var result = JSON.parse(this.responseText);

            for (let i = 0; i < result.length; i++) {

                let singerNodes = [];

                getSingersNode(result[i]['singer'], singerNodes).then(() => {

                    let singerlst = result[i]['singer'].split(".")
                    let singersStr = "<a href='./artist.html?id=" + singerlst[0] + "'>"  + getSingerNode(singerlst[0], singerNodes).name + "</a>"
                    for (let j = 1; j < singerlst.length; j++) {
                        singersStr += ", " + "<a href='./artist.html?id=" + singerlst[j] + "'>"  + getSingerNode(singerlst[j], singerNodes).name + "</a>"
                    }

                    //Update 1
                    if (i == 0) {

                        let title = document.getElementById("top-1-name");
                        title.innerText = result[i]["title"];
                        title.setAttribute('href', './song.html?id=' + result[i].id);
                        let thumbnail = document.getElementById("top-1-thumbnail");
                        thumbnail.src = result[i]["thumbnail"];
                        document.getElementById("top-1-href").setAttribute('href', './song.html?id=' + result[i].id);
                        document.getElementById("top-1-singer").innerHTML = singersStr;
                        document.getElementById("top-1-view").innerText = result[0]["view"];
                        // document.getElementById("top-1-cover").style.backgroundImage = "url('" + result[0]["cover"] +"')";
                    } else if (i == 1) {

                        let title = document.getElementById("top-2-name");
                        title.innerText = result[i]["title"];
                        title.setAttribute('href', './song.html?id=' + result[i].id);
                        let thumbnail = document.getElementById("top-2-thumbnail");
                        thumbnail.src = result[i]["thumbnail"];
                        document.getElementById("top-2-href").setAttribute('href', './song.html?id=' + result[i].id);
                        document.getElementById("top-2-singer").innerHTML = singersStr;
                        document.getElementById("top-2-view").innerText = result[1]["view"];
                    } else if (i == 2) {
                        let title = document.getElementById("top-3-name");
                        title.innerText = result[i]["title"];
                        title.setAttribute('href', './song.html?id=' + result[i].id);
                        let thumbnail = document.getElementById("top-3-thumbnail");
                        thumbnail.src = result[i]["thumbnail"];
                        document.getElementById("top-3-href").href='./song.html?id=' + result[i]["id"]
                        //thumbnail.setAttribute('href', './song.html?id=' + result[i]["id"]);
                        document.getElementById("top-3-singer").innerHTML = singersStr;
                        document.getElementById("top-3-view").innerText = result[2]["view"];
                    } else {
                        document.getElementById("top-list").innerHTML += " <div class='row'> <div class='col-1'> <div class='song-rank'>" + (i + 1) + "</div> </div> <div class='col-2'> <a class='song-ava-thumb' title='' href='./song.html?id=" + result[i]["id"] + "'> <img style='width:60px; height:60px' src='" + result[i]["thumbnail"] + "'> </a> </div> <div class='col-4'> <div class='song-detail'> <p class='title'> <a class='song-name'>" + result[i]["title"] + "</a> </p> <p class='sub-title'> <a class='song-name'>" + singersStr + "</a> </p> </div> </div> <div class='col-3'> <div class='tool-song'> <img class='tool-icon' src='../resources/icons/_ionicons_svg_md-add.svg' /> <img class='tool-icon' src='../resources/icons/_ionicons_svg_md-share.svg' /> <img class='tool-icon' src='../resources/icons/_ionicons_svg_md-videocam.svg' /> <img class='tool-icon' src='../resources/icons/_ionicons_svg_md-download.svg' /> </div> </div> <div class='col-2'> <div class='song-num'>" + result[i]["view"] + "</div> </div> </div> <br>"
                    }
                })
            }
        }
    }
    xmlhttp.open("GET", '../../servers/getChart.php', true);
    xmlhttp.send();
}


function getSingersNode(singerStr, singerNodes) {
    return new Promise((res, rej) => {
        let singersLst = singerStr.split(".")

        for (let i = 0; i < singersLst.length; i++) {
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    if (this.response != "") {

                        let artistInfo = JSON.parse(this.response)
                        singerNodes.push(artistInfo)
                    } else {
                        singerNodes.push({ id: i.toString(), name: singersLst[i] })
                    }
                    if (singerNodes.length == singersLst.length) {
                        res()
                    }
                }
            }

            xhttp.open("GET", "/bkmusic/servers/artist.php?id=" + singersLst[i], true);
            xhttp.send()
        }

    })

}

function getSingerNode(singerId, singerNodes) {
    for (let i = 0; i < singerNodes.length; i++) {

        if (singerId == singerNodes[i].id)
            return singerNodes[i]
    }
    return null
}