
function loadSingerDetail() {
    let url_string = window.location.href
    let url = new URL(url_string);
    let id = url.searchParams.get("id");
    return new Promise((res, rej) => {
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.response != "") {
                    res(JSON.parse(this.response))
                } else {
                    rej()
                }
            }
        }
        xhttp.open("GET", "/bkmusic/servers/artist.php?id=" + id, true);
        xhttp.send()
    })
}

function loadSinger() {
    loadSingerDetail().then((node) => {
        let name = document.getElementById('artist-name')
        let nation = document.getElementById('artist-nation')
        let bio = document.getElementById('artist-bio')
        let img = document.getElementById('artist-img')
        
        name.innerHTML = node.name
        nation.innerHTML = node.nation
        bio.innerHTML = node.description
        img.setAttribute('src', node.cover)
    })
}
function getSong(){
    var url = window.location.href;
    
    var searchTitle = decodeURI(url.split("=")[1]);
    $(location).attr('href', './artist-songs.html?search=' + searchTitle + "&page=1");
}
loadSinger()