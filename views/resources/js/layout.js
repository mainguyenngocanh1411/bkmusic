/**
 * Load navbar - header
 * Check user has login or not
 */
function includeHTML() {
    //Search button
    $("#search-butt").click(function () {
        var content = $("#search-input").val();

        $(location).attr('href', '../../views/layouts/search-result.html?search=' + content + "&page=1");
    });
    //payment button
    $('#payment').click(function(){
        $(location).attr('href', '../../views/layouts/payment.html');
    });
    //Load html - ref:w3school
    var z, i, elmnt, file, xhttp;
    /*loop through a collection of all HTML elements:*/
    z = document.getElementsByTagName("*");
    for (i = 0; i < z.length; i++) {
        elmnt = z[i];
        /*search for elements with a certain atrribute:*/
        file = elmnt.getAttribute("w3-include-html");
        if (file) {
            /*make an HTTP request using the attribute value as the file name:*/
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4) {
                    if (this.status == 200) { elmnt.innerHTML = this.responseText; }
                    if (this.status == 404) { elmnt.innerHTML = "Page not found."; }
                    /*remove the attribute, and call this function once more:*/
                    elmnt.removeAttribute("w3-include-html");
                    includeHTML();
                }
            }
            xhttp.open("GET", file, true);
            xhttp.send();
            /*exit the function:*/
            return;
        }
    }
    //check user login
    checkCookie();
};

/**
 * Check user hass login or not
 */
function checkCookie() {
    var button = document.getElementById("user-profile");
    if (getCookie("sessionString") != "") { //If sessionString exists then display profile button
        if (button != null) {
            
            button.innerHTML = "<img id='profile-ava' class='ava' width='50px' height='40px' src='../resources/icons/user.png'> <a href='./profile.html' class='name-log'>" + getCookie("name").replace("+", " ") + "</a> "
            $('#profile-ava').on("click", function () {//When click profile display logout button
                var detail = document.getElementById("user-detail").style.visibility;
                if (detail == "hidden") {
                    document.getElementById("user-detail").style.visibility = "visible"
                    $("#logout").on("click", function () {
                        deleteCookie("sessionString");
                        deleteCookie("PHPSESSID");
                        window.location.reload()
                    })
                }
                else {

                    document.getElementById("user-detail").style.visibility = "hidden"
                }

            })
        }




    }
    //else display signup button
}


/**
 * Support function
 * reference: w3school
 */
function deleteCookie(name) {
    document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

/**
 * Button sign up
 */
function register() {
    if (validate()) {
        
        $.ajax({
            type: 'POST',
            url: "../../servers/signup.php",
            data: {
                'name': document.getElementById('fullname').value,
                'mail': document.getElementById('mail').value,
                'psw': document.getElementById('psw').value,
            },

            success: function (data, textStatus, jqXHR) {
                if (data == "ok") {
                    //AUTO login
                    $.ajax({
                        type: 'POST',
                        url: "../../servers/login.php",
                        data: {
                            'mail': document.getElementById('mail').value,
                            'psw': document.getElementById('psw').value,
                        },

                        success: function (loginData, textStatus, jqXHR) {
                          
                            if (loginData == "ok") {
                                window.location.reload()
                            }

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert('Path Finder : ' + textStatus);
                        }
                    });

                }
                else {
                    document.getElementById("error").innerHTML = "<a style=color:red>" + data + "</a>";
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Path Finder : ' + textStatus);
            }
        });


    }
    else {
        console.log("not")
    }
}
function login() {
    $.ajax({
        type: 'POST',
        url: "../../servers/login.php",
        data: {
            'mail': document.getElementById('login-mail').value,
            'psw': document.getElementById('login-psw').value,
        },

        success: function (loginData, textStatus, jqXHR) {

            if (loginData == "ok") {
                window.location.reload()

            }
            else {
              
                
                document.getElementById("login-error").innerHTML = "<a style=color:red>" + loginData + "</a>";
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Path Finder : ' + textStatus);
        }
    });
}
function validate() {
    var mail = document.getElementById("mail").value;
    var pass = document.getElementById("psw").value;
    var repass = document.getElementById("repassword").value;
    let validPassword = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/g;
    let validEmail = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
    if (pass.match(validPassword)
        && repass == pass
        && mail.match(validEmail)) {

        return true;


    }
    else {
        if (!mail.match(validEmail)) {
            document.getElementById("error").innerHTML = "<a style=color:red> Mail is not valid </a>";
            return false;
        }
        else if (!pass.match(validPassword) || pass.length < 8) {
            document.getElementById("error").innerHTML = "<a style=color:red> Password has at least 8 chars, [0-9], [a-z] </a>";

            return false;

        }

        else if (repass != pass) {
            document.getElementById("error").innerHTML = "<a style=color:red> Retype password must be same as password </a>";

            return false;
        }
        else return false;

    }


}

/**
 * Search bar
 */
//live search
function search(e, str) {
    //search button
    if (!e) e = window.event;
    var keyCode = e.keyCode || e.which;
    if (keyCode == '13') {

        var content = $("#search-input").val();
        $(location).attr('href', '../../views/layouts/search-result.html?search=' + content + "&page=1");
    }
    var max_result = 9;

    if (str != "") {
        xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                // console.log(this.response)
                var result = JSON.parse(this.responseText);

                //clean result          
                document.getElementById("search-result").innerHTML = "";
                document.getElementById("search-result").style.visibility = 'visible';

                if (result.length == 0) {
                    document.getElementById("search-result").innerHTML = "Not found";

                }
                if (result.length > 10) {
                    var content = $("#search-input").val();
                 
                    document.getElementById("search-result").innerHTML += "<div class='more-result'> <a href='./search-result.html?search="+content +"&page=1' class='song-click'> More result </a></div>";
                    for (let i = 0; i < max_result; i++) {
                        let singerNodes = [];
                        getSingersNode(result[i]['singer'], singerNodes).then(() => {

                            let singerlst = result[i]['singer'].split(".")
                            let singersStr = getSingerNode(singerlst[0], singerNodes).name
                            for (let j = 1; j < singerlst.length; j++) {
                                singersStr += ", " + getSingerNode(singerlst[j], singerNodes).name
                            }
                            document.getElementById("search-result").innerHTML += "<div class='result-detail'><a href='./song.html?id=" + result[i]['id'] + "' class='song-click'>" + result[i]["title"] + "</a><br><a style='color:gray' class='result-singer'>" + singersStr + "</a></div>";
                        })
                    }
                } else {
                    for (let i = 0; i < result.length; i++) {
                        let singerNodes = [];
                        getSingersNode(result[i]['singer'], singerNodes).then(() => {

                            let singerlst = result[i]['singer'].split(".")
                            let singersStr = getSingerNode(singerlst[0], singerNodes).name
                            for (let j = 1; j < singerlst.length; j++) {
                                singersStr += ", " + getSingerNode(singerlst[j], singerNodes).name
                            }
                            document.getElementById("search-result").innerHTML += "<div class='result-detail'><a href='./song.html?id=" + result[i]['id'] + "' class='song-click'>" + result[i]["title"] + "</a><br><a style='color:gray' class='result-singer'>" + singersStr + "</a></div>";
                        })
                    }
                }


            }
        }
        xmlhttp.open("GET", "../../servers/livesearch.php?q=" + str, true);
        xmlhttp.send();
    }
    else {
        //clean result
        document.getElementById("search-result").remove;
        document.getElementById("search-result").style.visibility = 'hidden';

    }
    document.getElementById("body-full").addEventListener('click', function (event) {

        document.getElementById("search-result").remove;
        document.getElementById("search-result").style.visibility = 'hidden';
    });


}

function getSingersNode(singerStr, singerNodes) {
    return new Promise((res, rej) => {
        let singersLst = singerStr.split(".")

        for (let i = 0; i < singersLst.length; i++) {
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    if (this.response != "") {

                        let artistInfo = JSON.parse(this.response)
                        singerNodes.push(artistInfo)
                    } else {
                        singerNodes.push({ id: i.toString(), name: singersLst[i] })
                    }
                    if (singerNodes.length == singersLst.length) {
                        res()
                    }
                }
            }

            xhttp.open("GET", "/bkmusic/servers/artist.php?id=" + singersLst[i], true);
            xhttp.send()
        }

    })

}

function getSingerNode(singerId, singerNodes) {
    for (let i = 0; i < singerNodes.length; i++) {

        if (singerId == singerNodes[i].id)
            return singerNodes[i]
    }
    return null
}