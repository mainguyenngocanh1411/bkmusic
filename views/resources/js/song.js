var currentVolume = 0.5;
var artistId;
function showLyric() {
    let lyricContent = document.getElementById("lyric-content");
    let toggleBtn = document.getElementById("lyric-toggle-btn");
    if (lyricContent.style.height == "auto") {
        lyricContent.style.height = "300px";
        toggleBtn.innerText = "Show more";
    } else {
        lyricContent.style.height = "auto";
        toggleBtn.innerText = "Show less";
    }
}

function showControlPanel() {
    let controls = document.getElementById("controls");
    controls.classList.add('up');
}

function hideControlPanel() {
    let controls = document.getElementById("controls");
    controls.classList.remove('up');
}

function previous() {
    togglePreviousIc()
}

function togglePreviousIc() {
    let previousIc = document.getElementById('previous-ic');
    console.log(previousIc.src)
    if (previousIc.getAttribute('src') == '../resources/icons/previous-off.png') {
        previousIc.src = '../resources/icons/previous-on.png';
    } else {
        previousIc.src = '../resources/icons/previous-off.png';
    }
    console.log(previousIc.src)
}

function next() {
    toggleNextIc()
}

function toggleNextIc() {
    let nextIc = document.getElementById('next-ic');
    if (nextIc.getAttribute('src') == '../resources/icons/next-off.png') {
        nextIc.src = '../resources/icons/next-on.png';
    } else {
        nextIc.src = '../resources/icons/next-off.png';
    }
}

function loop() {
    let audio = document.getElementById('audio');
    if (audio.hasAttribute('loop')) {
        audio.removeAttribute('loop');
    } else {
        audio.setAttribute('loop', '');
    }
    console.log(audio.attributes)
    toggleLoopIc()
}

function toggleLoopIc() {
    let loopIc = document.getElementById('loop-ic');
    if (loopIc.getAttribute('src') == '../resources/icons/loop-off.png') {
        loopIc.src = '../resources/icons/loop-on.png';
    } else {
        loopIc.src = '../resources/icons/loop-off.png';
    }
    console.log(loopIc.getAttribute('src'))
}

function shuffle() {
    toggleShuffleIc()
}

function toggleShuffleIc() {
    let shuffleIc = document.getElementById('shuffle-ic');
    if (shuffleIc.getAttribute('src') == '../resources/icons/shuffle-off.png') {
        shuffleIc.src = '../resources/icons/shuffle-on.png';
    } else {
        shuffleIc.src = '../resources/icons/shuffle-off.png';
    }
    console.log(shuffleIc.src)
}

function onPause() {
    let songTheme = document.getElementById('song-theme');
    songTheme.classList.add('paused')
}

function onPlay() {
    let songTheme = document.getElementById('song-theme');
    songTheme.classList.remove('paused')
}

function like() {
    let likeIc = document.getElementById('like-ic');
    if (likeIc.getAttribute('src') == '../resources/icons/like.png') {
        likeIc.src = '../resources/icons/dislike.png';
    } else {
        likeIc.src = '../resources/icons/like.png';
    }
}

//Set's volume as a percentage of total volume based off of user click.
function setVolume(percentage) {
    currentVolume = percentage / 100
    document.getElementById('audio').volume = currentVolume;
    document.getElementById('volume-slider').setAttribute('style', 'background-image:-webkit-gradient( linear, left top, right top, color-stop(' + currentVolume + ', #33ff00), color-stop(' + currentVolume + ', #555));')
    editSpeaker(currentVolume);
}

function editSpeaker(volume) {
    let speaker = document.getElementById('speaker-ic');
    if (volume == 0) {
        speaker.src = '../resources/icons/speaker-off.png';
    } else if (volume < 0.6) {
        speaker.src = '../resources/icons/speaker-medium.png';
    } else {
        speaker.src = '../resources/icons/speaker-loud.png';
    }
}

function mute() {
    let audio = document.getElementById('audio');

    if (audio.volume != 0) {
        currentVolume = audio.volume;
        audio.volume = 0;
    } else {
        audio.volume = currentVolume;
    }
    editSpeaker(audio.volume);
}

function loadSong() {
    let xhttp = new XMLHttpRequest();
    let url_string = window.location.href
    let url = new URL(url_string);
    let id = url.searchParams.get("id");
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let songDetail = JSON.parse(this.response)

            loadSingersNode(songDetail.singer + "." + songDetail.composer).then(() => {

                loadSongContent(songDetail.title, songDetail.singer)
                loadSinger(songDetail.singer)
                loadComposer(songDetail.composer)

            })
            loadTitle(songDetail.title)
            loadLyric(songDetail.lyric)
            loadThumnail(songDetail.thumbnail)
            loadGenre(songDetail.genre)
        }
    };
    xhttp.open("GET", "/bkmusic/servers/song.php?id=" + id, true);
    xhttp.send()
}

function loadTitle(titleStr) {
    let title = document.createElement('title')
    title.innerHTML = titleStr
    document.head.appendChild(title)
    let titleInfo = document.getElementById('song-title-info')
    titleInfo.innerHTML = titleStr

    let songTheme = document.getElementById('song-theme')
    songTheme.setAttribute('alt', titleStr)
}

function loadLyric(lyric) {
    if (lyric != null) {

        let lyricContent = document.getElementById('lyric-content')
        lyricContent.innerHTML = lyric.replace(/&#10;/g, "<br/>")
    }
}

function loadSongContent(song, singerStr) {
    let songName = document.getElementById('song-name')
    let singerDiv = document.getElementById('artist-name')
    songName.innerHTML = song

    let singers = singerStr.split(".")
    if (singers.length != 0) {
        let firstSinger = createArtistContainer(singers[0]);
        singerDiv.appendChild(firstSinger)
        for (let i = 1; i < singers.length; i++) {
            singerDiv.appendChild(createComma())
            singerDiv.appendChild(createArtistContainer(singers[i]))
        }
    }
}

function loadThumnail(url) {
    let songTheme = document.getElementById('song-theme')
    songTheme.setAttribute('src', url)
}

function loadGenre(genre) {
    let tag = document.getElementById('genre-tag')
    let info = document.getElementById('genre-info')
    let sctag = document.getElementById('sc-tag')
    tag.innerHTML = genre
    sctag.setAttribute('href', './play-list.html?search=' + genre + '&page=1')
    info.innerHTML = genre
}

function loadComposer(composerStr) {
    let composers = composerStr.split(".")
    let composerDiv = document.getElementById('composer')
    if (composers.length != 0) {
        let firstComposer = createArtistContainer(composers[0]);
        composerDiv.appendChild(firstComposer)
        for (let i = 1; i < composers.length; i++) {
            composerDiv.appendChild(createComma())
            composerDiv.appendChild(createArtistContainer(composers[i]))
        }
    }
}

function loadSinger(singerStr) {
    let singers = singerStr.split(".")
    let singerDiv = document.getElementById('singer')
    if (singers.length != 0) {
        let firstSinger = createArtistContainer(singers[0]);
        singerDiv.appendChild(firstSinger)
        loadSingerDetail(singers[0])
        for (let i = 1; i < singers.length; i++) {
            singerDiv.appendChild(createComma())
            singerDiv.appendChild(createArtistContainer(singers[i]))
        }
    }
}

function loadSingerDetail(singerId) {
    let singerImg = document.getElementById('singer-image')
    let singerName = document.getElementById('singer-name')

    let singerNode = loadSingerNode(singerId)

    singerImg.setAttribute('src', singerNode.avatar)
    singerImg.setAttribute('alt', singerNode.name)
    singerName.innerText = singerNode.name
    artistId = singerId
}

var singersNode = [];

function loadSingersNode(singerStr) {
    return new Promise((res, rej) => {
        let singersLst = singerStr.split(".")

        for (let i = 0; i < singersLst.length; i++) {
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    if (this.response != "") {

                        let artistInfo = JSON.parse(this.response)
                        singersNode.push(artistInfo)
                    } else {
                        singersNode.push({ id: i.toString(), name: singersLst[i] })
                    }
                    if (singersNode.length == singersLst.length)
                        res()
                }
            }
            xhttp.open("GET", "/bkmusic/servers/artist.php?id=" + singersLst[i], true);
            xhttp.send()
        }
    })
}

function loadSingerNode(singerId) {
    for (let i = 0; i < singersNode.length; i++) {
        // console.log(parseInt(singerId) + " - " + singersNode[i].id + " - " + singersNode[i].name + ": " + (singerId == singersNode[i].id))

        if (singerId == singersNode[i].id)
            return singersNode[i]
    }
    return null
}

function createArtistContainer(singerId) {
    // console.log(singerId)
    let singerSpan = document.createElement('h2')

    let artistInfo = loadSingerNode(singerId)
    // console.log(artistInfo)
    singerSpan.classList.add('txt-primary')
    singerSpan.innerHTML = "<a style='color: coral;' href='./artist.html?id=" + singerId + "'>" + artistInfo.name + "</a>"

    return singerSpan
}

function createComma() {
    let comma = document.createElement('span')
    comma.classList.add('ft')
    comma.innerText = ', '
    return comma
}

function showArtistDialog() {
    // console.log('hi')
    let singerDialog = document.getElementById('singer-dialog')

    singerDialog.style.display = 'block'

    document.getElementById('singer-dialog-detail').setAttribute('src', './artist.html?id=' + artistId)



}
function updateView() {
    let url_string = window.location.href
    let url = new URL(url_string);
    let songId = url.searchParams.get("id");
    $.ajax({
        type: 'POST',
        url: "../../servers/updateView.php",
        data: {
            'id': songId
        },

        success: function (data, textStatus, jqXHR) {
            console.log(data)


        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Path Finder : ' + textStatus);
        }
    });

}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    let singerDialog = document.getElementById('singer-dialog')
    if (event.target == singerDialog) {
        singerDialog.style.display = 'none';
    }
}

function resizePopup() {
    let singerDialog = document.getElementById('singer-dialog-container')
    let ratio = singerDialog.clientWidth / window.outerWidth
    console.log(singerDialog.clientHeight)
    singerDialog.setAttribute('style', 'height:' + ratio * window.outerHeight + "px")
}

/**
 * Support function
 * Reference: w3school 
 */
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkSession() {
    
    $.ajax({
        type: 'POST',
        url: "../../servers/session.php",
        data: {
            'mail': getCookie("mail"),
            'sessionId': getCookie("PHPSESSID"),
            'sessionString': getCookie("sessionString"),
        },

        success: function (queryData, textStatus, jqXHR) {

            if (queryData == "") {
                document.getElementById("download-ctn").style.display = "none";
            }
            else {
                console.log(queryData)
                let user = JSON.parse(queryData)
                if (user.type != "Vip")
                document.getElementById("download-ctn").style.display = "none";
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Path Finder : ' + errorThrown);
            console.log(errorThrown)
        }
    });
}

loadSong()
updateView()
checkSession()