
window.onload = function () {
    getList()
}



function loadSinger(singerStr) {
    let singers = singerStr.split(".")
    let singerDiv = document.getElementById('singer')
    if (singers.length != 0) {
        let firstSinger = createArtistContainer(singers[0]);
        singerDiv.appendChild(firstSinger)
        loadSingerDetail(singers[0])
        for (let i = 1; i < singers.length; i++) {
            singerDiv.appendChild(createComma())
            singerDiv.appendChild(createArtistContainer(singers[i]))
        }
    }
}

/**
 * Get query from url then send to server to get data
 */
function getList() {


    var url = window.location.href;
    var query = url.split("&");
    var searchTitle = decodeURI(query[0].split("=")[1]);
    var searchPage = decodeURI(query[1].split("=")[1]);
    var sort = "title%20asc" //default sort
    if (query.length >= 3) {
        sort = decodeURI(query[2].split("=")[1])
    }
    //set title    
    document.title += "| " + searchTitle + " |page " + searchPage
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {

        if (this.readyState == 4 && this.status == 200) {
            var responseText = JSON.parse(this.responseText);

            var result = responseText['content']
            var actualLength = responseText['total']

            var currentPage = parseInt(searchPage);

            var total_page = Math.ceil(responseText['total'] / responseText['limit'])

            if (responseText['content'] != null) {
                if (searchTitle != '') {
                    document.getElementById("title-search").innerText = "'" + searchTitle + "'";
                }
                else {
                    document.getElementById("title-search").innerText = '';
                }

                document.getElementById("total").innerText = "Found " + actualLength + " results";
                if (responseText['total'] > responseText['limit']) {
                    /** Setup page bar */
                    //Set previous button
                    if (currentPage - 1 > 0) {
                        document.getElementById("page-bar").innerHTML += "<li class='page-item'><a class='page-link' href='../../views/layouts/search-result.html?search=" + searchTitle + "&page=" + (currentPage - 1) + "&sort=" + sort + "'>Previous</a></li>"
                    }

                    //Set page numbers
                    for (let i = 0; i < total_page; i++) {
                        document.getElementById("page-bar").innerHTML += "<li class='page-item'><a class='page-link' href='../../views/layouts/search-result.html?search=" + searchTitle + "&page=" + (i + 1) + "&sort=" + sort + "'>" + (i + 1) + "</a></li>"

                    }
                    //Set next button
                    if (currentPage + 1 <= total_page) {

                        document.getElementById("page-bar").innerHTML += "<li class='page-item'><a class='page-link' href='../../views/layouts/search-result.html?search=" + searchTitle + "&page=" + (currentPage + 1) + "&sort=" + sort + "'>Next</a></li>"
                    }


                }


                //list song
                for (let i = 0; i < result.length; i++) {
                    let singerNodes = [];
                    getSingersNode(result[i]['singer'], singerNodes).then(() => {

                        let singerlst = result[i]['singer'].split(".")
                        let singersStr = "<a href='./artist.html?id=" + singerlst[0] + "'>" + getSingerNode(singerlst[0], singerNodes).name + "</a>"
                        for (let j = 1; j < singerlst.length; j++) {
                            singersStr += ", " + "<a href='./artist.html?id=" + singerlst[j] + "'>" + getSingerNode(singerlst[j], singerNodes).name + "</a>"
                        }

                        document.getElementById("list-song").innerHTML += "<div class='item-song' id='' data-id='' data-sig='' data-code='' data-type=''> <div class='fl'> <a title='' class='thumb track-log' href='/bkmusic/views/layouts/song.html?id=" + result[i].id + "' order='' data-highlight=''> <img width='94' height='94' alt='thumbnail.pn' src='" + result[i]['thumbnail'] + "'> </a> </div> <div class='fl ml-20'> <div class='title-song'> <h2> <a class='nav-text' href=href='/bkmusic/views/layouts/song.html?id=" + result[i].id + "'> " + result[i]["title"] + "</a> </h2> </div> <div class='info-meta'><a>Singers: " + singersStr + "</a></div> <div class='info-meta'> <span>Thể loại:</span> <div class='inline'> <a class='nav-text' href='#'>" + result[i]["genre"] + "</a> <span class='bull'>•</span> Lượt nghe: <span class='' id='' data-id='' data-type=''>" + result[i]["view"] + "</span> </div> </div> </div> </div> <div class='clearfix'></div>"
                    })
                }

            }
            else {
                if (responseText['total'] != 0) {
                    document.getElementById("title-search").innerText = "'" + searchTitle + "'";
                    document.getElementById("total").innerText = "Found " + actualLength + " results";

                }
                else {
                    document.getElementById("title-search").innerText = "'" + searchTitle + "'";
                    document.getElementById("total").innerText = "Found " + actualLength + " results";
                }

            }

        }
    }
    xmlhttp.open("GET", "../../servers/search.php?filter=search:" + searchTitle + "&page:" + searchPage + "&sort:" + sort, true);
    xmlhttp.send();
}

function getSingersNode(singerStr, singerNodes) {
    return new Promise((res, rej) => {
        let singersLst = singerStr.split(".")

        for (let i = 0; i < singersLst.length; i++) {
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    if (this.response != "") {

                        let artistInfo = JSON.parse(this.response)
                        singerNodes.push(artistInfo)
                    } else {
                        singerNodes.push({ id: i.toString(), name: singersLst[i] })
                    }
                    if (singerNodes.length == singersLst.length) {
                        res()
                    }
                }
            }

            xhttp.open("GET", "/bkmusic/servers/artist.php?id=" + singersLst[i], true);
            xhttp.send()
        }

    })

}

function getSingerNode(singerId, singerNodes) {
    for (let i = 0; i < singerNodes.length; i++) {
        // console.log(parseInt(singerId) + " - " + singerNodes[i].id + " - " + singerNodes[i].name + ": " + (singerId == singerNodes[i].id))

        if (singerId == singerNodes[i].id)
            return singerNodes[i]
    }
    return null
}


/**
 * When click then sortKey and sorValue will be add to url
 */
function setSort() {
    var url = window.location.href;
    var query = url.split("&");
    var currentLink = query[0] + "&" + query[1];
    var nameAsc = document.getElementById("name-asc");
    var nameDesc = document.getElementById("name-desc");
    var singerAsc = document.getElementById("singer-asc");
    var singerDesc = document.getElementById("singer-desc");
    var views = document.getElementById("views");
    nameAsc.href = currentLink + "&sort=title%20asc";
    nameDesc.href = currentLink + "&sort=title%20desc";
    singerAsc.href = currentLink + "&sort=singer%20asc";
    singerDesc.href = currentLink + "&sort=singer%20desc";
    views.href = currentLink + "&sort=view%20desc"
}

/**
 * Show sort option
 */
function showSort() {


    if (document.getElementById("sort-list").style.visibility == 'visible') {
        document.getElementById("sort-list").style.visibility = 'hidden';
    }
    else {
        document.getElementById("sort-list").style.visibility = 'visible';
        setSort();
    }

}
